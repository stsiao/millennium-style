if(document.getElementById("ms-check-update")){
    var ajax = new XMLHttpRequest();
    ajax.open('get','https://gitee.com/api/v5/repos/stsiao/millennium-style/releases/latest');
    ajax.send();
    ajax.onreadystatechange = function () {
        if (ajax.readyState == 4 && ajax.status == 200) {
            var obj = JSON.parse(ajax.responseText);
            var newest = parseFloat(obj.tag_name);
            if(newest > MS_VER){
                document.getElementById("ms-check-update").style.removeProperty("display");
                document.getElementById("ms-ver").innerHTML = MS_VER;
                document.getElementById("ms-new-ver").innerHTML = obj.name;
                document.getElementById("ms-upd-dtl").innerHTML = obj.body;
                document.getElementById("ms-new-ver").innerHTML = obj.name;
                document.getElementById("ms-dwn-adr").href = obj.assets[0].browser_download_url;
            }
        }
    }
}