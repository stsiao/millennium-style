<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<?php $this->need('header.php'); ?>
<div class="col-mb-12 col-8" id="main" role="main">
    <article class="post" itemscope itemtype="http://schema.org/BlogPosting">
        <h1 class="post-title" itemprop="name headline">
            <a itemprop="url"
               href="<?php $this->permalink() ?>"><?php $this->title() ?></a>
        </h1>
        <ul class="post-meta">
            <li itemprop="author" itemscope itemtype="http://schema.org/Person">
                <a itemprop="name" href="<?php $this->author->permalink(); ?>" rel="author"><?php $this->author(); ?></a>
                &nbsp;发布于&nbsp;<time datetime="<?php $this->date('c'); ?>" itemprop="datePublished"><?php $this->date(); ?></time>
            </li>
            <li><?php _e('最后更新: '); ?>
                <time itemprop="datePublished"><?php echo date('Y-m-d',$this->modified); ?></time>
            </li>
            <li>
                <?php _e('另请参阅: ');
                $this->category(', ');
                if($this->tags) {
                    _e(', ');
                    $this->tags(', ');
                }
                ?>
            </li>
            <?php if ($GLOBALS['TPV_AKT']): ?>
            <li><?php $this->viewsNum();?> 次阅读</li>
            <?php endif ?>
        </ul>
        <div class="post-content" itemprop="articleBody">
            <?php $diff= round((time()- $this->modified) / 3600 / 24); if($diff>=100): ?>
                <div class="out-of-date"><i class="fas fa-exclamation-circle otd-icon"></i><span class="otd-font">&nbsp;本文最后一次在 <?php echo date('Y 年 n 月 j 日',$this->modified); ?>更新，部分内容可能已经过时！</span></div>
            <?php endif; ?>
            <?php
                $parsed = parseTOC(parseEmote($this->content));
                $GLOBALS['TOC_O'] = $parsed['toc'];
                echo $parsed['content']; 
            ?>
            <?php //echo parseEmote($this->content); ?>
        </div>
        <?php if ($this->fields->link && $this->fields->link!=''): ?>
        <div class="readOrigin"><p><a href="<?php echo $this->fields->link?>" target="_blank">阅读原文</a></p></div>
        <?php endif ?>
    </article>
    
    <?php $this->need('comments.php'); ?>

    <ul class="post-near">
        <li>上一篇: <?php $this->thePrev('%s', '没有了'); ?></li>
        <li>下一篇: <?php $this->theNext('%s', '没有了'); ?></li>
    </ul>
</div><!-- end #main-->

<?php $this->need('sidebar.php'); ?>
<?php $this->need('footer.php'); ?>
