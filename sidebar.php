<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<div class="col-mb-12 col-offset-1 col-3 kit-hidden-tb" id="secondary" role="complementary">
    <!-- TOC -->
    <?php if(($this->is('post')||$this->is('page')) && $GLOBALS['TOC_O']):?>
        <section class="widget" id="TOC">
            <h3 class="widget-title"><?php _e('导航'); ?></h3>
            <ul class="widget-list">
                <?php echo $GLOBALS['TOC_O']; ?>
            </ul>
        </section>
        
        <!-- 超范围后滚动 -->
        <section class="widget" id="TOC-scroll">
            <div class="popUpButton" onclick="popUpTOCSwitch()"><p><i class="fas fa-angle-double-left" id="TOC-scroll-sign"></i></p></div>
            <ul class="widget-list">
                <?php echo $GLOBALS['TOC_O']; ?>
            </ul>
        </section>
    <?php endif;?>
    
    <?php if (!empty($this->options->modelBlock) && in_array('ShowOther', $this->options->modelBlock)): ?>
    <!-- 随机文章 -->
        <section class="widget">
            <h3 class="widget-title"><?php _e('手气不错'); ?></h3>
            <ul class="widget-list">
                <?php outputRandomPosts($this->options->randompostcount); ?>
            </ul>
        </section>
    <?php endif; ?>

    <?php if (!empty($this->options->modelBlock) && in_array('ShowRecentComments', $this->options->modelBlock)): ?>
    <!-- 最近回复 -->
        <section class="widget">
            <h3 class="widget-title"><?php _e('最近回复'); ?></h3>
            <ul class="widget-list">
                <?php \Widget\Comments\Recent::alloc('ignoreAuthor=true')->to($comments); ?>
                <?php while ($comments->next()): ?>
                    <li>
                        <a href="<?php $comments->permalink(); ?>"><?php $comments->author(false); ?></a>: <?php $comments->excerpt(18, '...'); ?>
                    </li>
                <?php endwhile; ?>
            </ul>
        </section>
    <?php endif; ?>

    <?php if (!empty($this->options->modelBlock) && in_array('ShowCategory', $this->options->modelBlock)): ?>
    <!-- 文章分类 -->
        <section class="widget">
            <h3 class="widget-title"><?php _e('分类'); ?></h3>
            <?php \Widget\Metas\Category\Rows::alloc()->listCategories('wrapClass=widget-list'); ?>
        </section>
    <?php endif; ?>

</div><!-- end #sidebar -->
