<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>

        </div><!-- end .row -->
    </div>
    <!-- post图片放大 -->
    <?php if($this->is('post')): ?>
    <div id="picBox" style="display: none"><img id="zoomPic" /></div>
    <?php endif ?>
</div><!-- end #body -->

<footer id="footer" role="contentinfo">
    <?php if (!empty($this->options->modelBlock) && in_array('ShowArchive', $this->options->modelBlock)): ?>
    <p>文章归档</p>
    <ul class="post-meta"><?php \Widget\Contents\Post\Date::alloc('type=month&format=Y年n月')->parse('<li><a href="{permalink}">{date}</a></li>'); ?></ul>
    <?php endif ?>
    
    <?php if (!empty($this->options->socialInfo)):?>
    <p class="visible-tb">联系我</p>
    <p class="visible-tb social-footer">
        <?php /* 每行socialInfo为一组社交平台信息 */
        $socials = explode("\r\n", $this->options->socialInfo);
        foreach ($socials as $social):
            /* 按部分拆开 */
            $socialInfo = explode(',', $social);?>
            <a title="<?php echo $socialInfo[0]?>" target="_blank" href="<?php echo $socialInfo[1]?>"><i class="<?php echo $socialInfo[2]?>"></i><span>&nbsp;<?php echo $socialInfo[3]?></span></a>
        <?php endforeach ?>
    </p>
    <?php endif ?>
    <?php if (!empty($this->options->showLink)):?>
    <p>友情链接</p>
    <p class="social-footer">
        <?php /* 每行linkInfo为一组友链信息 */
        $links = explode("\r\n", $this->options->showLink);
        foreach ($links as $link):
            /* 按部分拆开 */
            $linkInfo = explode(',', $link);?>
            <a title="点击前往" target="_blank" href="<?php echo $linkInfo[1]?>"><?php echo $linkInfo[0]?></a>
        <?php endforeach ?>
    </p>
    <?php endif ?>
    <div class="site-search col-tb-4 col-tb-offset-4 visible-tb">
        <form id="search" method="post" action="<?php $this->options->siteUrl(); ?>" role="search">
            <label for="s" class="sr-only"><?php _e('搜索关键字'); ?></label>
            <input type="text" id="s" name="s" class="text" placeholder="<?php _e('输入关键字搜索'); ?>"/>
            <button type="submit" class="submit"><?php _e('<i class="fas fa-search"></i>'); ?></button>
        </form>
    </div>
    <p>&copy; <?php echo date('Y'); ?> <a href="<?php $this->options->siteUrl(); ?>"><?php $this->options->title(); ?></a>，使用 <a href="https://gitee.com/stsiao/millennium-style" target="_blank">Millennium-style主题</a>，由<a href="https://www.bwsl.wang">飞蚊话</a>基于官方主题制作。</p>
    <?php if (!empty($this->options->customFooter)):?>
    <p><?php echo $this->options->customFooter?></p>
    <?php endif ?>
</footer><!-- end #footer -->

<?php $this->footer(); ?>
<script src="<?php $this->options->themeUrl('owo/owo_custom.js'); ?>"></script>
<script>
var owo = new OwO({
    logo: 'owo表情',
    container: document.getElementsByClassName('OwO')[0],
    target: document.getElementById('textarea'),
    api: '<?php $this->options->themeUrl('owo/OwO_2.json'); ?>',
    position: 'down',
    width: '100%',
    maxHeight: '250px'
});
<?php if ($this->is('post')): ?>
addPicZoomListener();
<?php endif ?>
</script>
</body>
</html>
