<?php
/**
 * 一款我理想中的千禧年网页风格的Typecho主题。
 * 主题帮助：<a href="https://www.bwsl.wang/csother/140.html">Millennium-style，基于Typecho官方的实用主题</a>
 * 项目地址：<a href="https://gitee.com/stsiao/millennium-style">Gitee</a>
 *
 * @package Millennium-style
 * @author 飞蚊话
 * @version 3.0
 * @link https://www.bwsl.wang
 */

if (!defined('__TYPECHO_ROOT_DIR__')) exit;
$this->need('header.php');
?>

<?php if (!empty($this->options->stickPostID)): ?>
<!-- 置顶文章 -->
<h2 class="post-title top" itemprop="name headline">置顶文章</h2>
<?php
$stickPosts = explode(',', $this->options->stickPostID);
$stickPostsCount = count($stickPosts);
?>
<div class="<?php if($stickPostsCount <= 3): ?>row<?php endif; ?> stickPosts">
    <?php
    foreach($stickPosts as $stickPost):
    ?>
    <div class="col-mb-12 col-<?php echo ($stickPostsCount > 3)?4:12/count($stickPosts); ?> stickPost">
    <article class="post" itemscope itemtype="http://schema.org/BlogPosting"  style="border-bottom: 0;">
        <?php $this->widget('Widget_Archive@index_'.$stickPost, 'pageSize=1&type=post', 'cid='.$stickPost)->to($top); ?>
            <h2 class="post-title" itemprop="name headline">
                <a itemprop="url"
                    href="<?php $top->permalink() ?>"><?php $top->title() ?></a>
            </h2>
            <ul class="post-meta">
                <li><?php _e('发布时间: '); ?>
                    <time datetime="<?php $top->date('c'); ?>" itemprop="datePublished"><?php $top->date(); ?></time>
                </li>
                <li><?php _e('最后更新: '); ?><time itemprop="datePublished"><?php echo date('Y-m-d',$top->modified); ?></time></li>
                <li itemprop="interactionCount">
                    <a itemprop="discussionUrl"
                       href="<?php $top->permalink() ?>#comments"><?php $top->commentsNum('评论', '1 条评论', '%d 条评论'); ?></a>
                </li>
                <?php if($GLOBALS['TPV_AKT']): ?>
                <li><?php $top->viewsNum();?> 次阅读</li>
                <?php endif; ?>
            </ul>
            <div class="post-content" itemprop="articleBody">
                <?php $top->excerpt('100', '...'); ?><span class="readmore"><a itemprop="url" href="<?php $top->permalink() ?>">继续阅读 >></a></span>
            </div>
    </article>
    </div>
    <?php endforeach; ?>
</div>
<div class="col-12" role="main"><article class="post underline" itemscope itemtype="http://schema.org/BlogPosting"></article></div><!-- 分割线-->
<?php endif ?>

<!-- 最新文章 -->
<?php if (!empty($this->options->modelBlock) && in_array('ShowRecentPost', $this->options->modelBlock)): ?>
<div class="col-12" role="main">
    <article class="post" itemscope itemtype="http://schema.org/BlogPosting">
        <h2 class="post-title" itemprop="name headline">最近更新</h2>
        <?php \Widget\Contents\Post\Recent::alloc('pageSize=1')->to($recent);
            $this->widget('Widget_Archive@recent_view', 'pageSize=1&type=post', 'cid='.$recent->cid)->to($recent_view);?>
        <h2 class="post-title" itemprop="name headline">
            <a itemprop="url"
                href="<?php $recent->permalink() ?>"><?php $recent->title() ?></a>
        </h2>
        <div class="post-content" itemprop="articleBody">
            <?php $recent->content('- 阅读剩余部分 -'); ?>
        </div>
        <ul class="post-meta">
            <li><a href="<?php $recent->author->permalink(); ?>"><?php $recent->author(); ?></a> 发布于 <time datetime="<?php $recent->date('c'); ?>" itemprop="datePublished"><?php $recent->date(); ?></time>
            </li>
            <li><?php _e('另请参阅 '); ?><?php $recent->category(','); ?>,<?php $recent->tags(', ', true, ''); ?></li>
            <li itemprop="interactionCount">
                <a itemprop="discussionUrl"
                   href="<?php $recent->permalink() ?>#comments"><?php $recent->commentsNum('评论', '1 条评论', '%d 条评论'); ?></a>
            </li>
            <?php if($GLOBALS['TPV_AKT']): ?>
            <li><?php $recent_view->viewsNum(); ?> 次阅读</li>
            <?php TePostViews_Plugin::viewCounter($recent_view); ?>
            <?php endif; ?>
        </ul>
    </article>
</div>
<?php endif ?>

<!-- 分类文章 -->
<?php if (!empty($this->options->categoryslug)):
/* 将slug转换为数组，供输出 */
$cates = explode(',', $this->options->categoryslug);
/* 取分类表，并制作分类链接 */
$this->widget('Widget_Metas_Category_List@options')->to($categories);
$catepermalink = array();
while($categories->next()) {
    if (in_array($categories->slug, $cates)) {
        $catepermalink[$categories->slug] = $categories->permalink;
    }
}
/* 抓出的分类文章循环输出 */
foreach($cates as $cate): ?>
<div class="col-mb-12 col-4" id="main" role="main">
    <h2>分类 <a href="<?php echo $catepermalink[$cate] ?>"><?php echo metasInfo('name', $cate)?></a> 下的文章</h2>
    <ul class="widget-list">
        <?php $this->widget('Widget_Archive@index_'.$cate, 'pageSize=6&type=category', 'mid='.metasInfo('mid', $cate))->to($slug); ?>
        <?php while($slug->next()): ?>
            <li>
                <a itemprop="url" href="<?php $slug->permalink(); ?>"><?php $slug->title(); ?></a>
                <span>● <time datetime="<?php $slug->date('c'); ?>" itemprop="datePublished"><?php $slug->date(); ?></time></span>
            </li>
        <?php endwhile; ?>
        <li><span class="readmore"><a itemprop="url" href="/category/<?php echo $cate ?>">更多>></a></span></li>
    </ul>
</div>
<?php endforeach ?>
<div class="col-12" role="main"><article class="post underline" itemscope itemtype="http://schema.org/BlogPosting"></article></div><!-- 分割线-->
<?php endif ?>

<!-- 文章流 -->
<div class="col-mb-12 col-8" id="main" role="main">
    <?php while ($this->next()): ?>
        <article class="post" itemscope itemtype="http://schema.org/BlogPosting">
            <h2 class="post-title" itemprop="name headline">
                <a itemprop="url"
                   href="<?php $this->permalink() ?>"><?php $this->title() ?></a>
            </h2>
            <ul class="post-meta">
                <li itemprop="author" itemscope itemtype="http://schema.org/Person"><a itemprop="name" href="<?php $this->author->permalink(); ?>" rel="author"><?php $this->author(); ?></a> 更新于 <time itemprop="datePublished"><?php echo date('Y-m-d',$this->modified); ?></time>
                </li>
                <li><?php _e('分类: '); ?><?php $this->category(','); ?></li>
                <li itemprop="interactionCount">
                    <a itemprop="discussionUrl"
                       href="<?php $this->permalink() ?>#comments"><?php $this->commentsNum('评论', '1 条评论', '%d 条评论'); ?></a>
                </li>
                <?php if($GLOBALS['TPV_AKT']): ?>
                <li><?php $this->viewsNum();?> 次阅读</li>
                <?php endif; ?>
            </ul>
            <div class="post-content" itemprop="articleBody">
                <?php $this->excerpt(); ?>
            </div>
        </article>
    <?php endwhile; ?>

    <?php $this->pageNav('&laquo; 前一页', '后一页 &raquo;'); ?>
</div><!-- end #main-->

<?php $this->need('sidebar.php'); ?>
<?php $this->need('footer.php'); ?>
