window.onload=function() {

    if (localStorage.getItem("fwh_darkMode") == "dark") {
        colorTheme();
    }
    
    popUpTOC();
}

function colorTheme() {
    var i, tagList;
    if (document.documentElement.className == "dark-mode") {
        // 使用getElementByClassName去除所有dark-mode
        tagList = document.getElementsByClassName("dark-mode");
        for (i = tagList.length-1; i >= 0; i--) {
            tagList[i].classList.remove("dark-mode");
        }
        try {
            document.getElementById("TOC-scroll").style.backgroundColor = "";     // 单独赋值
        } catch (error) {
            console.log("[colorTheme] 没有浮动TOC");
        }
        localStorage.removeItem("fwh_darkMode");
    } else {
        document.documentElement.className = "dark-mode";
        document.getElementById("logo").classList.add("dark-mode");
        
        // a标签增加class
        tagList = document.getElementsByTagName("a");
        for(i = 0; i < tagList.length; i++) {
            tagList[i].classList.add("dark-mode");
        }
        
        // code标签增加class
        tagList = document.getElementsByTagName("code");
        for(i = 0; i < tagList.length; i++) {
            tagList[i].classList.add("dark-mode");
        }
        
        // blockquote标签增加class
        tagList = document.getElementsByTagName("blockquote");
        for(i = 0; i < tagList.length; i++) {
            tagList[i].classList.add("dark-mode");
        }
        
        // pre标签增加class
        tagList = document.getElementsByTagName("pre");
        for(i = 0; i < tagList.length; i++) {
            tagList[i].classList.add("dark-mode");
        }
        
        // comment-author类增加dark-mode
        tagList = document.getElementsByClassName("comment-author");
        for (i = 0; i < tagList.length; i++) {
            tagList[i].classList.add("dark-mode");
        }
        
        // coment-child类增加dark-mode
        tagList = document.getElementsByClassName("comment-child");
        for (i = 0; i < tagList.length; i++) {
            tagList[i].classList.add("dark-mode");
        }
        
        // 输入框增加dark-mode
        tagList = document.getElementsByTagName("input");
        for(i = 0; i < tagList.length; i++) {
            tagList[i].classList.add("dark-mode");
        }
        tagList = document.getElementsByTagName("textarea");
        for(i = 0; i < tagList.length; i++) {
            tagList[i].classList.add("dark-mode");
        }
        
        // 浮动TOC增加dark-mode
        try {
            document.getElementsByClassName("popUpButton")[0].classList.add("dark-mode");
            document.getElementById("TOC-scroll").style.backgroundColor = "#222";
        } catch (error) {
            console.log("[colorTheme] 没有浮动TOC");
        }
        
        // 翻页器增加dark-mode
        try {
            tagList = document.getElementsByClassName("page-navigator")[0].getElementsByTagName("li");  
            for (i = 0; i < tagList.length; i++) {
                tagList[i].classList.add("dark-mode");
            }
        } catch (error) {
            console.log("[colorTheme] 没有翻页器");
        }
        
        localStorage.setItem("fwh_darkMode", "dark");
    }
}

// 图片点击放大监听
function addPicZoomListener() {
    var articleList = document.getElementsByTagName("article");
    var imgList = [];
    for (var i = 0; i < articleList.length; i++) {
        imgList = imgList.concat(articleList[i].getElementsByTagName("img"));
    }
    for (i = 0; i < imgList.length; i++) {
        for (var j = 0; j < imgList[i].length; j++) {
            if (imgList[i][j].id == "zoomPic") {
                continue;
            }
            imgList[i][j].addEventListener("click", function() {
                document.getElementById("picBox").style.display = "";
                setTimeout(function() {document.getElementById("picBox").style.opacity = 1}, 1);
                document.getElementById("zoomPic").src = this.src;
                
            })
        }
    }
    
    document.getElementById("picBox").addEventListener("click", function() {
        document.getElementById("picBox").style.opacity = 0;
        setTimeout(function() {document.getElementById("picBox").style.display = "none"}, 500);
    })
}

function popUpTOC() {
    // 创建一个IntersectionObserver实例
    var observer = new IntersectionObserver((entries) => {
        entries.forEach(entry => {
            // 如果元素完全离开视窗，则触发
            if (entry.intersectionRatio <= 0) {
                document.getElementById("TOC-scroll").style.opacity = 1;
            }
            // 反之
            else {
                document.getElementById("TOC-scroll").style.opacity = 0;
            }
        });
    });
     
    // 开始观察元素
    try {
        observer.observe(document.getElementById("TOC"));
    } catch (error) {
        console.log("[popUpTOC] 没有TOC");
    }
}

function popUpTOCSwitch() {
    var transformX = document.getElementById("TOC-scroll").style.transform;
    if (transformX) {
        document.getElementById("TOC-scroll").style.transform = "";
        document.getElementById('TOC-scroll-sign').classList.replace("fa-angle-double-right", "fa-angle-double-left")
    }
    else {
        document.getElementById("TOC-scroll").style.transform = "translateX(0)";
        document.getElementById('TOC-scroll-sign').classList.replace("fa-angle-double-left", "fa-angle-double-right")
    }
}